# node-authentication

## Setup

#### Config

- create a file named dev.sqlite3

    ```bash
    touch dev.sqlite3
    ```

- rename `config.dist.js` to `config.js` and fill accordingly to your environment

    example:
    ```javascript
    module.exports = {
        jwt_secret: 'jhkoqpwsbw3ygdvnu856txhqc2d7dhk5x6',
        knex: {
            development: {
                client: 'sqlite3',
                connection: {
                    filename: './dev.sqlite3'
                }
            },

            staging: {
                client: 'postgresql',
                connection: {
                    database: 'my_db',
                    user: 'username',
                    password: 'password'
                },
                pool: {
                    min: 2,
                    max: 10
                },
                migrations: {
                    tableName: 'knex_migrations'
                }
            },

            production: {
                client: 'postgresql',
                connection: {
                    database: 'my_db',
                    user: 'username',
                    password: 'password'
                },
                pool: {
                    min: 2,
                    max: 10
                },
                migrations: {
                    tableName: 'knex_migrations'
                }
            }
        }
    }
    ```

#### Install

```bash
npm install
```

#### Run

```bash
npm run start
```