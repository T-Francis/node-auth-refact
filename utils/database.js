const knex = require('knex');
const config = require('../config');

module.exports = {
  database: knex(config.knex.development)
};
