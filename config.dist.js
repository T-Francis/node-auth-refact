module.exports = {
    jwt_secret: '',
    knex: {
        development: {
            client: '',
            connection: {
                filename: ''
            }
        },
        staging: {
            client: '',
            connection: {
                database: '',
                user: '',
                password: ''
            },
            pool: {
                min: 2,
                max: 10
            },
            migrations: {
                tableName: ''
            }
        },
        production: {
            client: '',
            connection: {
                database: '',
                user: '',
                password: ''
            },
            pool: {
                min: 2,
                max: 10
            },
            migrations: {
                tableName: ''
            }
        }
    }
}