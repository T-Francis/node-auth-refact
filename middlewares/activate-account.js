const bcrypt = require('bcrypt')
const { database } = require('../utils/database')

module.exports = async (req, res) => {

    let saltRounds = 5
    let hash = bcrypt.hashSync(req.body.password, saltRounds)

    await database('User').update({ active: 1, password: hash }).where({ email: req.body.email })

    res.redirect('/login')
}