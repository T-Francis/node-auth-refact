const jwt = require('jsonwebtoken')
const config = require('../config')

module.exports = (req, res) => {
    jwt.verify(req.query.token, config.jwt_secret, (err, payload) => {

        if (err) return res.send("Wrong token")

        console.log('JWT is valid and payload is\n', payload)
        const HTML = `<html>
                            <header>
                            </header>
                        
                            <body>
                                <p> Bonjour ${payload.fullname} </p>
                                <form action="/activate-account" method="post">
                                <div>
                                    <input type="hidden" name="email" value="${payload.email}">
                                </div>
                                    <div>
                                        <label for="msg">Your Password :</label>
                                        <input type="password" name="password">
                                    </div>
                                    <div class="button">
                                        <button type="submit">Validate</button>
                                    </div>
                                </form>
                            </body>
                        </html> `
        res.send(HTML)
    })
}