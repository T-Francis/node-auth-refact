const knex = require('knex')
const jwt = require('jsonwebtoken')

let config = require('../config.js')
let database = knex(config.knex.development)

module.exports = async (req, res) => {
  try {
    await database('User').insert(req.body)
  }
  catch {
    return res.status(404).json({ 'error': 'User already exist' })
  }
  // let token = jwt.sign(payload, secretOrPrivateKey, [options, callback])
  let token = jwt.sign({ fullname: req.body.fullname, email: req.body.email }, config.jwt_secret, { algorithm: 'HS256' })

  const ActivateURL = 'http://localhost:3000/confirm-registration?token=' + token
  const HTML = `<h1>Votre compte a été créé. </h1> <p> Veuillez <a href="${ActivateURL}" > cliquer ici </a>`
  res.send(HTML)
}